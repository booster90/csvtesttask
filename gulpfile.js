const { src, dest, parallel } = require('gulp');
const minifyCSS = require('gulp-minify-css');
const rename = require('gulp-rename');

function css() {
    return src('assets/*.css')
        .pipe(minifyCSS())
        .pipe(rename({suffix: '.min'}))
        .pipe(dest('assets/'))
}

exports.css = css;
exports.default = parallel(css);
