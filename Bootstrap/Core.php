<?php

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class Core
{
    const FILE_DIR = __DIR__ . '/files/';

    public static function view(string $viewName,  array $variables) {
        $configurationTwig = [];

        if (ENV === 'production') {
            $configurationTwig['cache'] = './cache/';
        }

        $loader = new FilesystemLoader(__DIR__ . '/../Views/');
        $twig = new Environment($loader, $configurationTwig);

        echo $twig->render($viewName, $variables);
    }

    public static function error404() {
        header("HTTP/1.0 404 Not Found");
        self::view('404.html.twig', ['title' => '404']);

        exit;
    }

    public static function error500(string $errorInfo) {
        header("HTTP/1.0 500 Internal Server Error");
        self::view('500.html.twig', ['title' => '500', 'info' => $errorInfo]);

        exit;
    }

    public static function downloadFile(string $fileName) {
        $path = self::FILE_DIR . $fileName;

        if (file_exists($path)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . basename($path) . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($path));
            readfile($path);

            exit;
        }
    }
}
