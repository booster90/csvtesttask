<?php

define('ENV', ''); //production

require 'vendor/autoload.php';

require_once __DIR__ . '/Bootstrap/Core.php';
require_once __DIR__ . '/Controllers/Controller.php';
require_once __DIR__ . '/Controllers/CsvController.php';

require_once __DIR__ . '/Models/Model.php';
require_once __DIR__ . '/Models/ModelCsv.php';

$csvController = new CsvController();


if (isset($_SERVER['PATH_INFO'])) {
    $pathSplit = explode('/', ltrim($_SERVER['PATH_INFO']));
} else {
    $pathSplit = '/';
}

/**
 * Routes
 */
if ($pathSplit === '/') {
    $csvController->index();

    return;
}

if ($pathSplit[1] === 'download' && isset($pathSplit[2])) {
    $csvController->download($pathSplit[2]);

    return;
}

Core::error404();
