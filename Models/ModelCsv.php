<?php


class ModelCsv extends Model
{
    public function valid(string $csvFilePath) {
        if (file_exists($csvFilePath)) {
            $arrayFromCsv = array_map('str_getcsv', file($csvFilePath));

            $sumArray = [];

            foreach ($arrayFromCsv as $row) {
                if (count($row) !== 3) {
                    return false;
                }

                if (!is_string($row[0]) ||  !is_numeric($row[1]) || !is_numeric($row[2])) {
                    continue;
                }

                $keyExists = false;
                $sum =  $row[1] * $row[2];

                foreach ($sumArray as &$item) {
                    if ($item['name'] === $row[0]) {
                        $keyExists = true;
                        $item['sum'] += $sum;
                    }
                }

                if (!$keyExists) {
                    $sumArray[] = ['name' => $row[0], 'sum' => $sum];
                }
            }

            return $sumArray;
        }

        return null;
    }
}
