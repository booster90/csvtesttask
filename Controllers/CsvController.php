<?php


class CsvController extends Controller {

    private $modelCsv = null;

    public function __construct() {
        parent::__construct();

        $this->modelCsv = new ModelCsv();
    }

    public function index() {
        if (is_array($_FILES) && isset($_POST['submit']) && isset($_FILES['fileToUpload'])) {
            $file = $_FILES['fileToUpload'];

            $csv = $this->modelCsv->valid($file['tmp_name']);

            if ($csv === false) {
                Core::error500('Uploaded file is not valid!');
            }

            $link = $this->generateCsv($csv);

            Core::view('uploaded.html.twig', [
                'title' => 'File has been uploaded',
                'fileName' => $file['name'],
                'header' => 'Upload csv file',
                'csv' => $csv,
                'link' => $link
            ]);

            return;
        }

        Core::view('index.html.twig', [
            'title' => 'load csv file',
            'header' => 'Upload csv file'
        ]);
    }

    public function download($fileName) {
        Core::downloadFile($fileName);
    }

    public function uploaded() {
        Core::view('index.html.twig', [
            'title' => 'load csv file',
            'header' => 'Upload csv file'
        ]);
    }

    private function generateCsv($csvData) {
        $link = sprintf('/files/%s.csv', date('Y-m-d'));

        $fp = fopen(__DIR__ . '/../' . $link, 'w');

        foreach ($csvData as $fields) {
            fputcsv($fp, array_values($fields));
        }

        fclose($fp);

        return $link;
    }
}
