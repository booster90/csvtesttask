$(document).ready(function () {
    'use strict'

    $('#fileToUpload').on('change', function(e) {
        if (e.target.files[0].type === 'text/csv') {
            $('#uploadFileLabel1').html('Added file (<b>' + this.files[0].name + '</b>)')
            $('#uploadCsvBtn').removeClass('d-none')

            return
        }

        $('#uploadFileLabel1').html(`File is a not csv file! (<b> ${this.files[0].name} </b>)`)
    })
});
